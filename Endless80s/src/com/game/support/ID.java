package com.game.support;

public enum ID {

	Player(),
	Trail(),
	FastEnemy(),
	SmartEnemy(),
	EnemyBoss(),
	MenuParticle(),
	HardEnemy(),
	Coin(),
	CoinShield(),
	CoinBomb(),
	BasicEnemy();
}
